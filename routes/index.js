const path = require('path');
const express = require('express');

const router = express.Router();

router.get('/', (req, res) => res.sendFile(path.join(__dirname, '../public/index.html')));
// router.get('/socket.io', (req, res) => res.json({}));
// router.post('/socket.io', (req, res) => res.json({}));

module.exports = router;
