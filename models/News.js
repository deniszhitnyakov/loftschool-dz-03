const mongoose = require('mongoose');

const { Schema } = mongoose;

const NewsSchema = new Schema({
  created_at: { type: Date, default: Date.now },
  text: String,
  title: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    index: true,
  },
});

NewsSchema.method('transform', function () {
  const obj = this.toObject();

  // Rename fields
  obj.id = obj._id;
  delete obj._id;

  return obj;
});

NewsSchema.virtual('id').get(function () {
  return this._id.toHexString();
});

NewsSchema.set('toJSON', {
  virtuals: true,
});

const News = mongoose.model('News', NewsSchema);

module.exports = News;
