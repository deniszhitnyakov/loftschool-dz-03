const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { Schema } = mongoose;

const UserSchema = new Schema({
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true },
  surName: { type: String },
  firstName: { type: String },
  middleName: { type: String },
  image: { type: String },
  permission: { type: Object },
});

UserSchema.method('transform', function () {
  const obj = this.toObject();

  // Rename fields
  obj.id = obj._id;
  delete obj._id;

  return obj;
});

UserSchema.pre('save', async function (next) {
  const user = this;

  if (!user.isModified('password')) return next();

  try {
    user.password = await bcrypt.hash(user.password, 10);
    return next;
  } catch (e) {
    return next(e);
  }
});

UserSchema.methods.comparePassword = async function (candidatePassword) {
  try {
    const result = await bcrypt.compare(candidatePassword, this.password);
    return result;
  } catch (_) {
    return false;
  }
};

UserSchema.virtual('id').get(function () {
  return this._id.toHexString();
});

UserSchema.set('toJSON', {
  virtuals: true,
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
