const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('passport');
const LocalStrategy = require('passport-local');
// const session = require('express-session');
// const FileStore = require('session-file-store')(session);
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const io = require('socket.io')();

const db = require('./db');
require('dotenv').config();
const UserModel = require('./models/User');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const apiRouter = require('./api/index');
const User = require('./models/User');

const app = express();
app.io = io;

io.on('connection', (socket) => {
  console.log('A user connected');
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('./public'));

app.use(db);

// Passport
// app.use(
//   session({
//     store: new FileStore(),
//     secret: 'secret',
//     resave: false,
//     saveUninitialized: true,
//   }),
// );
app.use(passport.initialize());
// app.use(passport.session());

passport.serializeUser((user, done) => done(null, user._id));

passport.deserializeUser((id, done) => {
  UserModel.findById(id, (err, user) => done(err, user));
});

// Локальная аутефикация
passport.use(new LocalStrategy(async (username, password, done) => {
  const user = await UserModel
    .findOne({ username });

  if (
    user !== null
    && await user.comparePassword(password)
  ) {
    return done(null, user);
  }

  return done(null, false);
}));

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET,
    },

    async (jwtPayload, done) => {
      try {
        const user = await User
          .findOne({ _id: jwtPayload.user._id })
          .select('-password');

        if (user === null) return done(null, false);

        return done(null, user);
      } catch (err) {
        return done(err, false);
      }
    },
  ),
);

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
