const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

async function init(req, res, next) {
  try {
    await mongoose.connect(
      'mongodb+srv://deniszhitnyakov:mandrake1992@cluster0.efubv.mongodb.net/loft?retryWrites=true&w=majority',
      {
        useCreateIndex: true,
        useUnifiedTopology: true,
        useNewUrlParser: true,
      },
    );

    return next();
  } catch (e) {
    return res.json({
      success: false,
      error: e,
    });
  }
}

module.exports = init;
