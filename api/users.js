const router = require('express').Router();
const UserModel = require('../models/User');

router.get(
  '/',

  async (req, res) => {
    try {
      const users = await UserModel
        .find()
        .select('-password');

      return res.json(users);
    } catch (err) {
      return res.json({ error: err });
    }
  },
);

router.delete(
  '/:id',

  async (req, res) => {
    try {
      await UserModel.deleteOne({ _id: req.params.id });
      return res.json({ success: true });
    } catch (err) {
      return res.json({ error: err });
    }
  },
);

router.patch(
  '/:id/permission',

  async (req, res) => {
    try {
      const user = await UserModel
        .findById(req.params.id)
        .select('-password');
      user.permission = req.body.permission;
      await user.save();

      delete user.password;

      return res.json(user);
    } catch (err) {
      return res.json({ error: err });
    }
  },
);

module.exports = router;
