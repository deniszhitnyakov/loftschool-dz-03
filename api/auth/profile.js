const router = require('express').Router();
const passport = require('passport');
const Joi = require('joi');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');

const UserModel = require('../../models/User');

router.get(
  '/profile',

  passport.authenticate('jwt'),

  (req, res) => res.json(req.user),
);

router.patch(
  '/profile',

  // Парсинг данных формы
  (req, res, next) => {
    const form = formidable({ multiples: true });
    form.parse(req, (err, fields, files) => {
      if (err) {
        return res.status(500).json({ error: err });
      }

      req.body = fields;
      req.files = files;

      return next();
    });
  },

  // Валидация
  async (req, res, next) => {
    const validationSchema = Joi.object({
      oldPassword: Joi.string().trim().optional(),
      newPassword: Joi.string().trim().optional(),
      surName: Joi.string().optional().allow(''),
      firstName: Joi.string().optional().allow(''),
      middleName: Joi.string().optional().allow(''),
    })
      .with('oldPassword', 'newPassword')
      .with('newPassword', 'oldPassword');

    try {
      req.body = await validationSchema.validateAsync(req.body);

      return next();
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  },

  // Авторизация
  passport.authenticate('jwt'),

  // Проверка смены пароля, если указан новый пароль
  async (req, res, next) => {
    if (!req.body.newPassword) return next();

    if (!req.body.oldPassword) {
      return res.status(500).json({ error: { message: 'No old password' } });
    }

    const user = await UserModel.findById(req.user._id);

    const oldPasswordMatch = await user.comparePassword(req.body.oldPassword);
    if (!oldPasswordMatch) {
      return res.status(500).json({ error: { message: 'Incorrect old password' } });
    }

    return next();
  },

  // Подчищаем пароли
  (req, res, next) => {
    if (req.body.oldPassword) delete req.body.oldPassword;
    if (req.body.newPassword) {
      req.body.password = req.body.newPassword;
      delete req.body.newPassword;
    }

    return next();
  },

  // Сохранение аватарки, если есть
  async (req, res, next) => {
    if (!req.files.avatar) return next();

    try {
      fs.copyFileSync(
        req.files.avatar.path,
        path.join(
          __dirname,
          '../../public/avatars',
          `${req.user._id}${path.extname(req.files.avatar.name)}`,
        ),
      );

      fs.rmSync(req.files.avatar.path);

      req.body.image = `/avatars/${req.user._id}${path.extname(req.files.avatar.name)}`;

      return next();
    } catch (err) {
      return res.json({ error: err });
    }
  },

  // Сохранение данных в БД
  async (req, res) => {
    try {
      const user = await UserModel.findById(req.user._id);

      Object
        .keys(req.body)
        .forEach((field) => {
          user[field] = req.body[field];
        });

      await user.save();

      return res.json(user);
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  },
);

module.exports = router;
