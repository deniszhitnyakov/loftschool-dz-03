const router = require('express').Router();
const jwt = require('jsonwebtoken');
const Joi = require('joi');

require('dotenv').config();

const db = require('../../db');
const UserModel = require('../../models/User');

router.post(
  '/registration',

  // Валидация
  async (req, res, next) => {
    const validationSchema = Joi.object({
      username: Joi.string().trim().required(),
      password: Joi.string().trim().required(),
      surName: Joi.string().optional().allow(''),
      firstName: Joi.string().optional().allow(''),
      middleName: Joi.string().optional().allow(''),
    });

    try {
      req.body = await validationSchema.validateAsync(req.body);

      return next();
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  },

  // Подключение к БД
  db,

  // Проверка на существование имени пользователя
  async (req, res, next) => {
    try {
      const user = await UserModel.findOne({ username: req.body.username });
      if (user === null) return next();

      return res.status(500).json({ error: { message: 'Username is already taken' } });
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  },

  // Регистрация
  async (req, res) => {
    try {
      const user = await UserModel.create({
        ...req.body,
        permission: {
          settings: {
            C: true,
            R: true,
            U: true,
            D: true,
          },
          chat: {
            C: true,
            R: true,
            U: true,
            D: true,
          },
          news: {
            C: true,
            R: true,
            U: true,
            D: true,
          },
        },
      });

      // await user.save();

      return res.json(user);
    } catch (err) {
      return res.status(500).json({
        error: err,
      });
    }
  },
);

module.exports = router;
