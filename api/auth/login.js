const router = require('express').Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const Joi = require('joi');

require('dotenv').config();

router.post(
  '/login',

  // Валидация
  async (req, res, next) => {
    const validationSchema = Joi.object({
      username: Joi.string().trim().required(),
      password: Joi.string().trim().required(),
    });

    try {
      req.body = await validationSchema.validateAsync(req.body);

      return next();
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  },

  // Авторизация
  passport.authenticate('local'),

  // Выдача токенов
  (req, res) => {
    const accessTokenExpiredAt = Math.floor(
      Date.now() + process.env.JWT_EXPIRES_IN * 1000,
    );
    const refreshTokenExpiredAt = Math.floor(
      Date.now() + process.env.JWT_REFRESH_EXPIRES_IN * 1000,
    );

    const response = {
      ...req.user._doc,

      accessToken: `Bearer ${jwt.sign(
        { user: req.user },
        process.env.JWT_SECRET,
        { expiresIn: accessTokenExpiredAt },
      )}`,

      refreshToken: `Bearer ${jwt.sign(
        { user: req.user },
        process.env.JWT_REFRESH_SECRET,
        { expiresIn: refreshTokenExpiredAt },
      )}`,

      accessTokenExpiredAt,
      refreshTokenExpiredAt,
    };

    return res.json(response);
  },
);

module.exports = router;
