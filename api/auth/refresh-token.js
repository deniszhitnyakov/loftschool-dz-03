const router = require('express').Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');

require('dotenv').config();

router.post(
  '/refresh-token',

  // Авторизация
  passport.authenticate('jwt'),

  // Выдача токенов
  (req, res) => {
    const accessTokenExpiredAt = Math.floor(
      Date.now() / 1000 + process.env.JWT_EXPIRES_IN,
    );
    const refreshTokenExpiredAt = Math.floor(
      Date.now() / 1000 + process.env.JWT_REFRESH_EXPIRES_IN,
    );

    const response = {
      accessToken: jwt.sign(
        { user: req.user },
        process.env.JWT_SECRET,
        { expiresIn: accessTokenExpiredAt },
      ),

      refreshToken: jwt.sign(
        { user: req.user },
        process.env.JWT_REFRESH_SECRET,
        { expiresIn: refreshTokenExpiredAt },
      ),

      accessTokenExpiredAt,
      refreshTokenExpiredAt,
    };

    return res.json(response);
  },
);

module.exports = router;
