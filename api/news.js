const router = require('express').Router();
const NewsModel = require('../models/News');
const UserModel = require('../models/User');

router.get(
  '/',

  async (req, res) => {
    try {
      const news = await NewsModel
        .find()
        .populate('user', 'firstName middleName surName username image');

      return res.json(news);
    } catch (err) {
      return res.json({ error: err });
    }
  },
);

router.post(
  '/',

  async (req, res, next) => {
    try {
      await NewsModel.create({
        user: req.user._id,
        title: req.body.title,
        text: req.body.text,
      });

      return next();
    } catch (err) {
      return res.json({ error: err });
    }
  },

  async (req, res) => {
    try {
      const news = await NewsModel
        .find()
        .populate('user', 'firstName middleName surName username image');

      return res.json(news);
    } catch (err) {
      return res.json({ error: err });
    }
  },
);

router.delete(
  '/:id',

  async (req, res, next) => {
    try {
      await NewsModel.deleteOne({ _id: req.params.id });
      return next();
    } catch (err) {
      return res.json({ error: err });
    }
  },

  async (req, res) => {
    try {
      const news = await NewsModel
        .find()
        .populate('user', 'firstName middleName surName username image');

      return res.json(news);
    } catch (err) {
      return res.json({ error: err });
    }
  },
);

router.patch(
  '/:id',

  async (req, res, next) => {
    try {
      await NewsModel.updateOne(
        { _id: req.params.id },
        { text: req.body.text, title: req.body.title },
      );
      return next();
    } catch (err) {
      return res.json({ error: err });
    }
  },

  async (req, res) => {
    try {
      const news = await NewsModel
        .find()
        .populate('user', 'firstName middleName surName username image');

      return res.json(news);
    } catch (err) {
      return res.json({ error: err });
    }
  },
);

module.exports = router;
