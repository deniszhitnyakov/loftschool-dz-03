const router = require('express').Router();

// Авторизация и аутефикация
router.use(require('./auth/login.js'));
router.use(require('./auth/registration.js'));
router.use(require('./auth/profile.js'));
router.use(require('./auth/refresh-token.js'));

// Пользователи
router.use('/users', require('./users.js'));

// Новости
router.use('/news', require('./news.js'));

module.exports = router;
